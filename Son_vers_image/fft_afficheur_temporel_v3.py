import math
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile as wave
from numpy.fft import fft

from matplotlib.animation import FuncAnimation

#Defining useful constants
#rate,data = wave.read('piano_la3.wav')
rate,data = wave.read('transe.wav')

n = data.size//2
div = 1000
dt = n//div

#Degrading wave date from stero to mono
data_mono = []
for i in data:
    data_mono.append(i[0])


def flore(n):
    return min(73e6, n*30)


# #Initializing figure
# spectre = np.absolute(fft(data_mono[0:dt-1]))
# spectre = spectre/spectre.max()
# 
# n = spectre.size
# freq = np.zeros(n)
# for k in range(n):
#     freq[k] = 1.0/n*rate*k

# 
# #Affichage
# fig = plt.figure()
# line = plt.vlines(spectre,[0],freq,'r')
# 
# plt.axis([0,44100,10000,1000000])
# plt.yscale('log')


#Initialisation matrice
hauteur = 2000
img = np.zeros((hauteur,div))

# 
# def init():
#     line = plt.vlines(spectre,[0],freq,'r')
#     return line,
# 
# spec = np.zeros([1,10000])+0.5
# 
# def animate(i):
#     start = i*dt
#     stop = start + dt-1
#     spectre = np.absolute(fft(data_mono[start:stop]))
# #    spectre = spectre/spectre.max()
#     
#     n = spectre.size
#     freq = np.zeros(n)
#     for k in range(n):
#         freq[k] = 1.0/n*rate*k
#     
#     line = plt.vlines(freq,[0],spectre)
#     
#     return line,

def remplir(i):
    """Rempli la colonne i de la matrice"""
    start = i*dt
    stop = start + dt-1
    spectre = np.absolute(fft(data_mono[start:stop],n=22050))
#    spectre = spectre/spectre.max()
    
    n = spectre.size
    freq = np.zeros(n)
    for k in range(n):
        freq[k] = 1.0/n*rate*k
    
    #line = plt.vlines(freq,[0],spectre)
    for k in range(0,hauteur):
        img[k,i]=flore(spectre[k])
    
    return 

for i in range(div):
    remplir(i)
    if i%100 == 0:
        print(i)

plt.imshow(img, interpolation= None, cmap=plt.cm.bwr
, aspect = 1/5.0)

#ani = FuncAnimation(fig, animate, init_func = init, frames = div, blit =True, interval = 20, repeat = True)

# # Set up formatting for the movie files
# Writer = FuncAnimation.writers['ffmpeg']
# writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)
# 
# ani.save('spectro1.mp4', writer=writer)

plt.show()
    